# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Attribute Set Mass Edit",
    "version": "2.0.1.1.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/odoo-pim",
    "depends": ["attribute_set", "mass_editing", "onchange_helper"],
    "data": ["views/attribute_attribute.xml"],
    "demo": [],
    "installable": True,
}
