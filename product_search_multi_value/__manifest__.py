# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Search Multi Value",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/odoo-pim",
    "depends": ["product"],
    "data": ["data/search_field_data.xml", "views/product_template_view.xml"],
    "demo": [],
}
