# Flectra Community / odoo-pim

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[attribute_set_mass_edit](attribute_set_mass_edit/) | 2.0.1.1.0| Attribute Set Mass Edit
[attribute_set](attribute_set/) | 2.0.1.3.2| Attribute Set
[product_search_multi_value](product_search_multi_value/) | 2.0.1.0.1| Product Search Multi Value
[product_attribute_set](product_attribute_set/) | 2.0.1.1.0| Product Attribute Set
[attribute_set_completeness](attribute_set_completeness/) | 2.0.2.0.0| Attribute Set Completeness
[attribute_set_searchable](attribute_set_searchable/) | 2.0.1.0.1| Attribute Set Searchable
[pim](pim/) | 2.0.1.1.0| Product Information Management
[product_attribute_set_completeness](product_attribute_set_completeness/) | 2.0.2.0.0| Product Attribute Set Completeness


